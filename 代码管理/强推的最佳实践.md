# 强推的最佳实践

## 前言

对于开发者而言，使用 git 时，强推容易造成远程服务器上的分支数据丢失，容易造成生产事故。
但有些时候非强推不可。

## 什么时候需要强推

在一些情况下，强推非常必要，比如一下情况：
1.   当不慎把一些账户密码，密钥，开发者 Token 提交到 git 存储库并推送到远程服务器上时。
2.   当 Gitee 上存储库较大要缩小存储库体积时。
3.   使用 [gitgitgadget](https://github.com/gitgitgadget/gitgitgadget) 给 git 提交 PR 时。
4.   一些其他情况下需要变更特定分支的最新 commit。

### 1. 使用强推避免泄漏关键数据

此处关键数据指用户账户资料，密码，密钥，开发者 Token 或一些其他不能公开的数据。

强推的第一种情况是将本地的存储库已纳入的关键数据从存储库中过滤后重新提交（一般使用 `git filter-branch`），然后强制推送到远程服务器，避免关机数据的泄漏。在 git clone 或者 fetch 的时候，git 获取远程服务器上的数据必然是通过引用列表回溯计算获得所需的对象，在通过强制推送后，关键数据已经变成了不可达的数据（git 悬空对象），在 git gc 之后，便会被删除，Gitee 目前提供了存储库 GC 功能，因此，如果发现存储库包含一些关键数据，应当及时强推然后运行存储库 GC，避免信息泄漏造成生产事故。

### 2. 通过强推缩减存储库体积

几乎所有的代码托管平台支持的存储库体积都是有限的，Gitee 也不例外，存储库大小限制可以参考：[https://gitee.com/help/articles/4232](https://gitee.com/help/articles/4232)。

但存储库体积超出限制时，通常可以需要使用 `git filter-branch` 修改历史，这个过程将大文件排除在存储库的管理，然后使用强推推送到 Gitee 服务器，并运行 Git GC 缩减存储库体积。

保持较小体积的存储库对提高 Git clone push 等操作的用户体验非常必要。

### 3. 通过 gitgitgadget 给 git 提交 PR

使用 gitgitgadget 给 git 提交 PR 时，如果不使用强推，如果一个 PR 有多个 commit，则会提交多个 PATCH 文件到 git 邮件列表，如果功能并不复杂，则严重影响 Review 的体验，此时将多个 commit 合并为一个然后强推到 gitgitgadget，这样就只有一个 PATCH。这样就不用当心多个 PATCH 影响体验了。

### 4. 其他强推场景

这种情况就属于自定义了，一千个读者有一千个哈姆雷特，但切忌不要扩大化强制推送的使用范围。

## 禁止强推的代价

Gitee 提供了禁止强推的配置，但禁止强推对于一个提交有大量 commit 时，会导致 push 时间变长，这一过程主要是 git 需要计算此次 push 是否属于强制推送。在 git 传输协议中并没有强制推送的标志，在本地推送没有添加 `--force` 标志时，本地的 git 会比较本地特定的引用和远程引用判断是否可推送，无法推送则报告错误，可以则直接传输数据；而开启 `--force` 便会跳过检查支持将数据传输到远程服务器。因此，服务器上并不知道客户端的数据是否是强推的，智能在开启禁止强推的标志后进行计算判断是否属于强推。因此在开启强推后，次次推送都增加了计算过程，这一过程或许比使用其他流程规范代价更大。

## 避免强推的开发流程

避免开发过程中的强推需要规范开发流程，因为即使是设置了禁止强推，如果用户有心修改，一样可能会破坏存储库数据，造成生产事故。因此应当使用其他的策略避免这种情况的发生。

好的策略可以是多分支，使用保护分支，只读分支避免开发者修改，只允许关键人员进行修改，也可以使用 PR 模型，所有的提交都通过 PR 合并到主存储库。


## 强推时恢复数据

如果不慎强制推送到远程服务器上，修改了特定数据，如果本机也发生过 `reset` `rebase` 这样到操作，可以在 `git log` 中查看特定的 commit， 然后通过新建引用的方式恢复数据，但如果不慎覆盖他人的 commit，可以联系此人，让其恢复，git 是一个分布式版本控制系统，其他人有拷贝时即可恢复。如果被强制推送覆盖的数据只在特定的服务器上，比如 Gitee。那此时你需要联系 Gitee 的运维人员并申请授权其恢复你的数据。当然，上述的前提是没有 GC，GC 一时爽，一直爽啊一直爽，GC 之后再无退路。
