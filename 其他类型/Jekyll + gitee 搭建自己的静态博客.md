Jekyll + gitee 搭建自己的静态博客
---  

现在有很多静态博客框架，比如说 `Jekyll`，`Hexo`等，为什么选 `Jekyll` 呢？ 因为我最先遇到它啊！  

在码云上使用 `Jekyll` 搭建静态博客很简单，没有操作系统的限制，不用安装编译环境，只要创建一个符合 `Jekyll` 规则的仓库，开启 `pages` 服务，一个博客网站就可以使用了。  

我们先看看 Jekyll [官方文档](https://www.jekyll.com.cn/docs/)中介绍的目录结构，  

```
├── _config.yml
├── _data
|   └── members.yml
├── _drafts
|   ├── begin-with-the-crazy-ideas.md
|   └── on-simplicity-in-technology.md
├── _includes
|   ├── footer.html
|   └── header.html
├── _layouts
|   ├── default.html
|   └── post.html
├── _posts
|   ├── 2007-10-29-why-every-programmer-should-play-nethack.md
|   └── 2009-04-26-barcamp-boston-4-roundup.md
├── _sass
|   ├── _base.scss
|   └── _layout.scss
├── _site
├── .jekyll-metadata
└── index.html 
```  

其中   
- `_config.yml` 项目配置文件，可以在这里配置网站根目录，网站简介，时区，编码格式，markdown渲染等。
- `_data` 存放网站目录，支持的格式有 `.yml`, `.yaml`, `.json`, `.csv`, `.tsv`等。
- `_drafts` 存放草稿
- `_includes` `layouts` 和 `posts` 通过标签 `{% include file.ext %}` 引用的文件
- `_layouts` 模板文件
- `_posts` 发布的文章
- `_sass` 样式表文件
- `_site` jekyll 编译后网站文件存放目录，在码云，我们用不到这个文件夹
- `.jekyll-metadata` jekyll 编译配置文件，官方文档上说，这个文件帮助 Jekyll 识别哪些文件的更改需要 track ，哪些文件要在下次编译的时候重新生成。  
- `index.html` 网站的主页，也可以是 `index.md`

上面这些我们并不需要全部，我们只需要下面这些：  

```
├── _config.yml
├── _includes
|   ├── footer.html
|   └── header.html
├── _layouts
|   ├── default.html
|   └── post.html
├── _posts
|   ├── 2007-10-29-why-every-programmer-should-play-nethack.md
|   └── 2009-04-26-barcamp-boston-4-roundup.md
├── _sass
|   ├── _base.scss
|   └── _layout.scss
└── index.html 
```  

就可以使我们的网站正常工作了。  

了解了目录结构，简单阅读一下文档，我们就可以在码云创建我们的网站仓库了。  

在网上找一个自己喜欢的网站主题，主题有很多，可以根据自己的喜好选择 ，比如说 [Hux Blog](https://github.com/Huxpro/huxpro.github.io)，

修改一下 `_config.yml` 中的 `baseurl:` 选项，比如说这样：  

```
baseurl: ./projectname
```

之后就可以用类似 `username.gitee.io/projectname` 的 URL 访问我们的网站了。其中 `username` 使我们在 码云 的个性地址，`projectname` 是我们的仓库名称，如果 `projectname` 跟 `username` 相同。  我们让`baseurl`为空，就可以使用URL `username.gitee.io`访问我们的网站了。

网站可以访问后，就是写文章了，使用`markdown`写文章是一件很舒服的事情啊。

更舒服的是使用 码云 的在线IDE

![IDE](https://images.gitee.com/uploads/images/2019/0604/171719_63ac4e1d_120012.png "微信截图_20190531132308.png")
如图，在`_posts`目录下面新建文章，文件名称格式要统一，像下面这样：  

```
YYYY-MM-DD-title.md
```

写好文章了提交，稍等一分钟，就可以在网站上欣赏自己的大作了。

有兴趣，还可以根据自己的喜好定制网站的主题和样式。