# 利用 docsify 构造动态项目文档网站

## docsify 简介

docsify 是一个动态生成文档网站的工具。不同于 GitBook、Hexo 的地方是它不会生成将 .md 转成 .html 文件，所有转换工作都是在运行时进行。

## 搭建

1. 在 Gitee 上创建一个仓库，并克隆到本地。
2. 根目录下新建 index.html 文件，引入 docsify.js。
    ```code html
    <!DOCTYPE html>
        <html lang="zh">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/docsify@4.9.4/themes/vue.css">
            <title>项目文档名</title>
        </head>
        <body>
            <div id="app"></div>
            <script>
                window.$docsify = {
                    loadSidebar : true,
                    loadNavbar: true,
                    subMaxLevel: 2,
                    name : "项目文档名",
                    homepage : "index.md",
                }
            </script>

            <script src="https://cdn.jsdelivr.net/npm/docsify@4.9.4/lib/docsify.min.js"></script>
        </body>
        </html>
    ```
3. 目录结构
    ```
    docs
    ├─ index.md        => http://domain.com
    ├─ guide.md         => http://domain.com/guide  
    └─ hugo-guide       
        └─ guide.md     => http://domain.com/hugo-guide/guide
    ```
4. 构建侧边栏
    - 首先在 ```window.$docsify``` 中添加 loadSidebar 参数，并设置为 true。
    - 创建侧边栏文件 ```_sidebar.md```。
    ```
    - [首页](/)
    - [指南](/hugo-guide/guide.md)  
    ```
5. 定制导航栏
    - 首先在 ```window.$docsify``` 中添加 loadNavbar 参数，并设置为 true。
    - 创建导航栏文件 ```_navbar.md```，并添加相应内容即可
    ```
    - [关于](/about.md)
    ```
6. 为了阻止忽略下划线开头的文件，需要在根目录下创建 ```.nojekll``` 文件。

## 部署到码云

码云部署非常方便，直接帮本地克隆仓库推送到码云上，并开启 Gitee Pages 服务即可。
